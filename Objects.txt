Conference
	List<Track> lstOfTracks
	List<Talk> lstOfTalks
	Conference()
	Conference(List<Talk> talks)
	addTracks(int number)
	
Track
	List<Session>
	addSession(SessionType type)
	getFirstAvailableSession()
	isFull()

SessionType {MORNING(latestStart, latestEnd), LUNCH(12,13), EVENING(13,17), NETWORKING(17,18)}

Session
	List<Talk> lstOfTalks
	SessionType type
	actualtStart
	actualEndTime
	addTalk(Talk t)
	removeTalk(Talk t)
	isFull()
	updateActualTimes()

Talk
	String title
	int timeInMins
	Track track
	Session session
	isSlottable(Session s)
	setTrack(Track t)
	setSession(Session s)

EventManager
	List<Talk> 	readTalkDetails()
	Conference 	createConference(List<Talk> lstOfTalks)
	void 		organise(Conference c) // Overview of implementation given at the last
	void 		printSchedule(Conference c)

SessionOverflowException
	Thrown when a session cannot accomodate a talk
TrackOverflowException
	Thrown when a track cannot accomodate a session
NonTalkSessionException
	Thrown if a track is tried to be added to a non talk session
DuplicateSessionException
	Thrown if a same type of session is added to a track more than once
TimeExceededException
	Thrown when a session's start time is after the latest start or end time is after the latest start/end time respectively

// Implementation is based on FCFS, take a talk and try to fit it in the curren track
// Definitely better options would be available, this method can be updated to implement those
organise(Conference conf)
	For each talk in the conference
		If no current track or curr track if full, add a track to conference
		Try to add the talk to the next available session of the current track
		Update the talk with the session and track information
	At last, update the NETWORKING session's start time as per the EVENING session of each track
	Also update the actual times for each session